﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopyHelper
{
    public class CopyFile : INotifyPropertyChanged
    {
        private string _pathName;
        private string _fullName;
        private string _fileName;

        public string PathName
        {
            get { return _pathName; }
            set{ if(_pathName != value){
                _pathName = value;
                OnPropertyChanged("PathName");
            }} }

        public string FullName
        {
            get { return _fullName; }
            set{ if(_fullName != value){
                _fullName = value;
                OnPropertyChanged("FullName");
            }} }

        public string FileName
        {
            get { return _fileName; }
            set{ if(_fileName != value){
                _fileName = value;
                OnPropertyChanged("FileName");
            }} }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
