﻿using Hardcodet.Wpf.TaskbarNotification;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace CopyHelper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<CopyFile> selectedFiles = new ObservableCollection<CopyFile>();

        public ObservableCollection<CopyFile> SelectedFiles
        {
            get { return selectedFiles; }
        }

        public Action CopyCommand
        {
            get
            {
                return () => { Button_Click_1(this, new RoutedEventArgs()); };
            }
        }
        private TaskbarIcon tb;

        private ICommand _removeItem;

        public ICommand RemoveItem
        {
            get
            {
                return _removeItem ?? (_removeItem = new RelayCommand(p =>
                    RemoveItemCommand((CopyFile)p), p => true));
            }
        }

        private ICommand _setItemPath;

        public ICommand SetItemPath
        {
            get
            {
                return _setItemPath ?? (_setItemPath = new RelayCommand(p =>
                    SetItemPathCommand((CopyFile)p), p => true));
            }
        }

        private ICommand _copyFile;

        public ICommand CopyFile
        {
            get
            {
                return _copyFile ?? (_copyFile = new RelayCommand(p =>
                    CopySingleFile((CopyFile)p), p => true));
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            this.tb = (TaskbarIcon)FindResource("NotifyIcon");
            this.tb.Icon = Resource1.copy;
            this.Left = SystemParameters.PrimaryScreenWidth - this.Width;
            this.Top = SystemParameters.WorkArea.Height - this.Height;
        }

        private void Rectangle_Drop(object sender, System.Windows.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(System.Windows.DataFormats.FileDrop);
                HandleFilesOpen(files);
            }
            
        }

        private void HandleFilesOpen(string[] files)
        {
            foreach (var f in files)
            {
                var lastSlash = f.LastIndexOf('\\');
                var filename = f.Substring(lastSlash + 1);
                selectedFiles.Add(new CopyFile { FullName = f, FileName = filename });
            }
        }

        public void RemoveItemCommand(CopyFile item)
        {
            var i = selectedFiles.First(x => x.FullName == item.FullName);
            selectedFiles.Remove(i);
        }

        public void SetItemPathCommand(CopyFile item){
            var i = selectedFiles.First(x => x.FullName == item.FullName);

            var dialog = new CommonOpenFileDialog(){ IsFolderPicker = true };
            if (dialog.ShowDialog().ToString().ToLower() == "ok")
            {
                i.PathName = dialog.FileName;
            }
        }

        public void CopySingleFile(CopyFile item)
        {
            try
            {
                string curFile = System.IO.Path.GetFileName(item.FullName);
                string newPathToFile = System.IO.Path.Combine(item.PathName, curFile);
                File.Copy(item.FullName, newPathToFile);
            }
            catch (Exception)
            {
                //lazy
                this.tb.ShowBalloonTip("Fail!", "Error! Maybe no path?", BalloonIcon.Error);
                return;
            }

            this.tb.ShowBalloonTip("Success!", "The File has been copied!", BalloonIcon.Info);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if(!this.selectedFiles.Any()){
                System.Windows.MessageBox.Show("No files to copy!");
                return;
            }

            int count = 0;
            foreach (var f in this.selectedFiles.Where(x => !String.IsNullOrWhiteSpace(x.PathName)))
            {
                CopySingleFile(f);
                count++;
            }

            if (count > 1)
            {
                this.tb.ShowBalloonTip("Success!", string.Format("{0} Files copied!", count), BalloonIcon.Info);
            }
            else if (count > 0)
            {
                this.tb.ShowBalloonTip("Success!", "The File has been copied!", BalloonIcon.Info);
            }
            else
            {
                this.tb.ShowBalloonTip("Fail!", "No Files to copy (check paths).", BalloonIcon.Warning);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
        private void Window_Deactivated(object sender, EventArgs e)
        {
            var chbx = (System.Windows.Controls.CheckBox)this.FindName("StayOnTopChkBx");
            this.Topmost = chbx.IsChecked ?? false;
        }
    }
}
