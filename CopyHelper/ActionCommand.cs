﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace CopyHelper
{
    public class ActionCommand : ICommand
    {
        public void Execute(object parameter)
        {
            ((MainWindow)App.Current.Windows[0]).CopyCommand();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}
